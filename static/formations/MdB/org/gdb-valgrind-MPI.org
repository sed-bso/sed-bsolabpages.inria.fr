* Espace de travail :
** connexion à une des machines suivantes : loki agni vulcain hephaistos
** ~/<login>/{src,bin,test}
** compilation depuis src
#+BEGIN_SRC 
   make install
#+END_SRC

** exécution depuis test
** suggestion d'avoir un screen :
*** un screen a été créé par utilisateur
*** pour le réattacher, il faut tapper :
#+BEGIN_SRC
   screen -dr <login>
#+END_SRC

*** C-a C-a pour changer de fenêtre

* Utilisation de gdb avec MPI :
** -gdb avec mpich2 (ma version, déconseillé mais fonctionne très bien)
*** avant la compilation, lors du configure il faut mettre option --with-pm=mpd,hydra
*** exemple :
#+BEGIN_SRC 
   mpirun -gdb -n 4 ../bin/ring
   r <args>
#+END_SRC
    
** une console par proc MPI (Vincent Perrier) :
#+BEGIN_SRC 
   mpirun -n 4 xterm -sb -geometry 200x50+0+900 -sl 1000 -hold -e gdb --eval-command=run --args ../bin/ring
#+END_SRC

** un screen par proc MPI (Xavier Lacoste) : http://bfroehle.com/2011/09/14/debugging-mpi-python/

** option « detach-on-fork » de gdb ?
*** est-ce qu'elle pourrait fonctionner avec MPI ?


** une barrière MPI (François Pellegrini) :
#+BEGIN_SRC
   if (rank == 0) {
     char           c;

     printf ("Waiting for key press...\n");
     scanf ("%c", &c);
   }
   MPI_Barrier (comm);
#+END_SRC


** ddt (sur PlaFRIM)

* Utilisation de valgrind avec MPI :
** compiler valgrind
*** MPI doit être en bibliothèque dynamique
**** option --enable-shared pour MPICH
*** Mettre la dépendance vers MPI
**** --with-mpicc=/opt/mpich2-1.5rc3-mpd-hydra/bin/mpicc
*** vérifier la ligne dans la sortie du configure afin que le wrapper puisse
    être bien généré
#+BEGIN_SRC 
checking primary target for usable MPI2-compliant C compiler and mpi.h...  yes, /opt/mpich2-1.5rc3-mpd-hydra/bin/mpicc
checking secondary target for usable MPI2-compliant C compiler and mpi.h...  no
#+END_SRC

** Premier exemple :
#+BEGIN_SRC 
    mpirun -n 4 valgrind ../bin/ring
#+END_SRC

*** Trop d'erreurs de MPI
**** la solution est de précharger le wrapper de valgrind pour MPI à l'aide de LD_PRELOAD
#+BEGIN_SRC 
    LD_PRELOAD=/opt/valgrind-3.9.0/lib/valgrind/libmpiwrap-amd64-linux.so mpirun -n 4 valgrind ../bin/ring
#+END_SRC

*** Les sorties de valgrind sont par processeur et il peut être fastidieux de
    retrouver ses petits lorsque le nombre de processeurs devient important. Il
    est nécessaire de trouver aisément quel processeur est à l'origine de quelle
    sortie.
**** afficher la correspondance entre processus MPI et numéro de processus au
     niveau système
#+BEGIN_SRC 
  char procnam[MPI_MAX_PROCESSOR_NAME] ;
  int  namelen;


  MPI_Get_processor_name(procnam,&namelen);
  printf ("Processus %d, host: %s, with MPI rank %d on %d\n", getpid(), procnam, world_rank, world_size);
#+END_SRC
#+BEGIN_SRC 
     LD_PRELOAD=/opt/valgrind-3.9.0/lib/valgrind/libmpiwrap-amd64-linux.so mpirun -n 4 valgrind ../bin/ring2
#+END_SRC
   
*** trop de sorties qui peuvent être imbriquées les unes aux autres
**** rediriger sortie standard ou d'erreur par proc mpi
     ce qui permet également de voir sur certains clusters les sorties en temps
     réel (comme Avakas)
#+BEGIN_SRC 
  #include <fcntl.h>
  ...
  int bak, new;
  char s[50];
  ...
  fflush (stdout);
  bak = dup (1);
  sprintf (s, "out_%d", world_rank);
  new = open(s, O_CREAT|O_WRONLY|O_TRUNC, 0600);
  dup2 (new, 1);
  ...
  fflush (stdout);
  dup2 (bak, 1);
  close (bak);
#+END_SRC
#+BEGIN_SRC 
     LD_PRELOAD=/opt/valgrind-3.9.0/lib/valgrind/libmpiwrap-amd64-linux.so mpirun -n 4 valgrind ../bin/ring3
#+END_SRC

** si la compilation de valgrind n'est pas suffisante pour les erreurs de MPI
*** utiliser les fichiers de suppressions d'erreurs
**** attention à être au plus près des erreurs que vous voulez supprimer, sinon
     vous risquez de ne plus voir certaines de vos erreurs
**** un exemple dans l'installation de valgrind :
     /opt/valgrind-3.9.0/lib/valgrind/default.supp
**** la possibilité de les générer à la volée :
#+BEGIN_SRC
     valgrind --leak-check=full --show-reachable=yes --error-limit=no --gen-suppressions=all --log-file=minimalraw.log ./minimal
#+END_SRC
**** pour plus d'infos : https://wiki.wxwidgets.org/Valgrind_Suppression_File_Howto

** sur PlaFRIM :
*** il faut rajouter après mpirun : « --mca btl tcp,self »

* mpi avec valgrind et gdb
** options --vgdb=yes --vgdb-error=1
*** quelle version de gdb ?
*** quelle version de valgrind ?
** ligne à mettre dans ~/.gdbinit
#+BEGIN_SRC
   define vg
   target remote | vgdb --pid=$arg0
   end
   document vg
   Start a vgdb relay application with specified pid process (arg0)
   end
#+END_SRC

** premier exemple :
*** décommenter la partie 51 à 58 de ring3.c
*** à faire dans un terminal :
#+BEGIN_SRC 
   LD_PRELOAD=/opt/valgrind-3.9.0/lib/valgrind/libmpiwrap-amd64-linux.so mpirun -n 4 valgrind --vgdb=yes --vgdb-error=0 ../bin/ring3
#+END_SRC

*** chercher le numéro de proc <p> dans un fichier out_*
*** dans un autre terminal depuis test (en remplaçant <p> par le numéro de proc identifié ci-dessus) :
#+BEGIN_SRC 
   gdb ../bin/ring3
   vg <p>
#+END_SRC

** deuxième exemple :
*** à faire dans un terminal :
#+BEGIN_SRC 
   LD_PRELOAD=/opt/valgrind-3.9.0/lib/valgrind/libmpiwrap-amd64-linux.so mpirun -n 4 valgrind --vgdb=yes --vgdb-error=3 ../bin/ring3
#+END_SRC

*** chercher le numéro de proc <p> dans un fichier out_*
*** dans un autre terminal depuis test (en remplaçant <p> par le numéro de proc identifié ci-dessus) :
#+BEGIN_SRC 
   gdb ../bin/ring3
   vg <p>
#+END_SRC

*** gdb nous indique la ligne où valgrind a identifié une erreur
