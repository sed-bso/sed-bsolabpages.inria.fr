# Les midis de la bidouille (de la donnée à sa visualisation)

Inria Bordeaux Sud-ouest

herve.mathieu@inria.fr

## Sommaire

* Présentation des technologies
* Serveurs mis en place
* Exemples
* Travaux pratiques (A vous de jouer)

--- 

## Présentation des technologies

ELK stack
E — Elasticsearch, L — Logstash, K — Kibana

* Logstash is an open source, server-side data processing pipeline
* Elasticsearch is a distributed, RESTful search and analytics engine (NoSQL)
* Kibana is an open source analytics and visualisation platform

<img src="https://cdn-images-1.medium.com/max/1600/1*OHP01Lidop3GQZbnwg9s4Q.jpeg" alt="ELK" width="400"/>

--- 

## Serveurs mis en place

### Installation sur réseau Inria

https://kibana.bordeaux.inria.fr/

https://elasticsearch.bordeaux.inria.fr/

Aucun système d'authentification n'est mis en place, donc l'ensemble des données est accessible (en lecture et en écriture) par toute personne accédant aux serveurs.

Règle d'usage : afin d'éviter tout écrasement accidentel de données et permette aux administrateurs de tracer l'usage des serveurs, 
il est demandé aux utilisateurs de préfixer leur index (ElasticSearch indices) par nom_de_l_equipe:sujet_de_l_etude (et éventuellement des sous-objet séparés par : ). 

**Cas particulier pour les midis de la bidouille : mdb:nom\_de\_famille**

--- 


### Possibilité d'une installation en local sur sa machine

#### Elasticsearch
1. https://www.elastic.co/downloads/elasticsearch : download elasticsearch-6.6.1.tar.gz (voir version actuelle)
2. gunzip elasticsearch-6.6.1.tar.gz
3. cd elasticsearch-6.6.1; ./bin/elasticsearch
4. in your favorite web browser: http://localhost:9200/ 

#### Kibana
1. https://www.elastic.co/downloads/kibana : download kibana-6.6.1.tar.gz (voir version actuelle)
2. gunzip kibana-6.6.1.tar.gz
3. cd kibana-6.6.1; ./bin/kibana
4. in your favorite web browser: http://localhost:5601/ launches the Kibana interface

--- 

## Exemples

Typologie des exemples :

1. des données sous forme : csv, json, texte
2. un programme en python pour peupler la base Elasticsearch
3. des propositions de visualisations sur Kibana

exemples :

* nuage de mot (texte)
* commit sur un projet git (curl, json)
* données temporelles de plafrim (csv)
* données météo (json, TP mdb)

--- 

## A vous de jouer

--- 

### Récupération des exemples

* mkdir mdb\_elk; cd mdb\_elk
* git clone git@gitlab.inria.fr:sed-bso/elasticsearch_examples.git


### meteo_mdb (contexte) 

Des données météorologiques accessibles :

https://www.infoclimat.fr/api-previsions-meteo.html?id=07510&cntry=STA

Pour visualiser le json :

```
python -m json.tool meteo.json
```
--- 

### Proposition d'environnement de travail virtuel dans un terminal :

* sudo apt install virtualenv
* mkdir virtualenv; cd virtualenv; 
* virtualenv -p python3 env
* source virtualenv/env/bin/activate

Pour les paquets python requis :
* pip install elasticsearch
* pip install requests

si problème IPV4/IPV6 :
```
$ sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
$ sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
```
--- 

### Code à compléter (meteo.py)

#### es\_index unique

es\_index = 'mdb:mon_nom'

#### mapping_input

mapping : https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html

Les possibilités : text, keyword, date, long, double, boolean, ip

cas particulier des dates : https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-date-format.html#built-in-date-formats

Choisir les champs que vous souhaitez afficher

Tester la lecture du JSON avant d'envoyer les données sur Elasticsearch

--- 

### Kibana

Une fois les données envoyées avec succès

Aller sur kibana.bordeaux.inria.fr

#### index Kibana

Management -> Kibana -> Index Patterns -> Create index pattern

step 1/2 : index pattern : mettre le nom de son index

step 2/2 : option (date)

#### Visualisation

plus(+) -> Line -> mon index

Y-axis -> Max -> pression

X-axis -> Date Histogram -> date -> Hourly

En haut à droite : penser à changer le fenêtre temporelle -> Last 7 days

--- 

## Pour aller plus loin

* Tableau de bord (agrégat de visualisation)
* Logstash (pas essayé)
* Vega (kibana en script) : kibana -> visualise -> tadaam:demo\_git\_vega


## Sources (liens)

* https://www.elastic.co/
* https://medium.freecodecamp.org/how-to-use-elasticsearch-logstash-and-kibana-to-visualise-logs-in-python-in-realtime-acaab281c9de
* https://yhatt.github.io/marp/ (markdown vers présentation PDF)






