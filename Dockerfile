FROM ubuntu:24.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y
RUN apt-get install -y ca-certificates git python-is-python3 wget
RUN apt-get autoremove -y
RUN apt-get autoclean -y
RUN apt-get purge -y
RUN update-ca-certificates

RUN wget https://github.com/gohugoio/hugo/releases/download/v0.69.0/hugo_0.69.0_Linux-64bit.deb
RUN dpkg -i hugo_0.69.0_Linux-64bit.deb
