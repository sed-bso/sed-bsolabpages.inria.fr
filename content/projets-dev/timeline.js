function loadTimeline(jsonfile,projectName,zoomMax)
{
    if (zoomMax == undefined)
	zoomMax = 25;
    // Load your JSON file using AJAX
    var xhr = new XMLHttpRequest();
    xhr.open("GET", jsonfile, true);
    xhr.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");
    xhr.setRequestHeader("Expires", "Tue, 01 Jan 1980 1:00:00 GMT");
    xhr.setRequestHeader("Pragma", "no-cache");
    xhr.onreadystatechange = function () {
	if (xhr.readyState == 4 && xhr.status == 200) {
	    // Parse JSON data
	    var data = JSON.parse(xhr.responseText);
	    var groups = new vis.DataSet();
	    var items = new vis.DataSet();

	    if (data.leaderitems.length != 0)
	    {
		groups.add({ "order": 10, "id": "leader", "content": "" });
		data.leaderitems.forEach(function(item) { item.group = "leader"; item.className = "leader";});
		items.add(data.leaderitems);
	    }

	    if (data.releasesitems.length != 0)
	    {
		groups.add({ "order": 20, "id": "releases", "content": "Releases" });
		data.releasesitems.forEach(function(item) { item.group = "releases"; item.type = "point"; });
		items.add(data.releasesitems);
	    }

	    if (data.postdocitems.length != 0)
	    {
		groups.add({ "order": 70, "id": "PostDoctorants", "nestedGroups": ["postdoc"], "showNested": false });
		groups.add({ "id": "postdoc", "content": "" });
		data.postdocitems.forEach(function(item) { item.group = "postdoc";});
		items.add(data.postdocitems);
	    }

	    if (data.masteritems.length != 0)
	    {
		groups.add({ "order": 50, "id": "Master Thesis", "nestedGroups": ["master"], "showNested": false });
		groups.add({ "id": "master", "content": "" });
		data.masteritems.forEach(function(item)	{ item.group = "master";});
		items.add(data.masteritems);
	    }

	    if (data.internitems.length != 0)
	    {
		groups.add({ "id": "interns", "content": "" });
		groups.add({"order": 60, "id": "Students", "nestedGroups": ["interns"], "showNested": false });
		data.internitems.forEach(function(item) { item.group = "interns"; });
		items.add(data.internitems);
	    }

	    if (data.usingitems.length != 0)
	    {
		groups.add({ "order": 80, "id": "Using " + projectName, "nestedGroups": ["using"], "showNested": false });
		groups.add({ "id": "using", "content": "" });
		data.usingitems.forEach(function(item) { item.group = "using";});
		items.add(data.usingitems);
	    }

	    if (data.phditems.length != 0)
	    {
		groups.add({ "order": 40, "id": "PhD Thesis", "nestedGroups" : ["phd"], "showNested": true });
		groups.add({ "id": "phd", "content": "" });
		data.phditems.forEach(function(item) { item.group = "phd"; });
		items.add(data.phditems);
	    }

	    if (data.engitems.length != 0)
	    {
		groups.add({ "order": 30, "id": "Engineers", "nestedGroups": ["eng"], "showNested": false });
		groups.add({ "id": "eng", "content": "" });
		data.engitems.forEach(function(item) { item.group = "eng"; });
		items.add(data.engitems);
	    }

	    if (data.fundingitems.length != 0)
	    {
		groups.add({ "order": 90, "id": "funding", "content": "Funding"});
		data.fundingitems.forEach(function(item) { item.className = "funding"; item.group = "funding"; });
		items.add(data.fundingitems);
	    }

	    var container = document.getElementById("visualization");
	    var options = {
		tooltip: {followMouse: true, overflowMethod: "cap"},
		orientation: {axis: "top", item: "bottom"},
		showCurrentTime: false,
		width: "100%",
		locale: "en",
		zoomMin: 1000 * 60 * 60 * 24 * 31 * 12,    // 1 year (12 months) in milliseconds
		zoomMax: 1000 * 60 * 60 * 24 * 31 * 12 * zoomMax, // about x years in milliseconds
	    };

	    function widthDays(date1, date2) {
		var one_day=1000*60*60*24;
		var date1_ms = new Date(date1).getTime();
		var date2_ms = new Date(date2).getTime();
		var difference_ms = date1_ms - date2_ms;
		return Math.round(difference_ms/one_day) / 30;
	    }

	    function setTitles(nodes) {
		nodes.forEach(function(node) {
		    if (node.content != undefined) {
			var width=50;
			if (node.end != undefined) {
			    width = widthDays(node.end, node.start);
			    if (width < 50) {
				width = 50;
			    }
			}
			var array = node.content.split(" ");
			node.content = "";
			var length = 0;
			for (let i = 0; i < array.length; i++) {
			    node.content += array[i] + " ";
			    length += array[i].length + 1;
			    if (length > width) {
				node.content += "<br/>";
				length = 0;
			    }
			}
		    }
		});

		nodes.forEach(function(node) {
		    node.title = "";
		    if (node.content != undefined) {
			node.title += "<H3>" + node.content + "</h3>";
		    }
		    if (node.description != undefined) {
			node.title += "<H4>" + node.description + "</h4>";
		    }
		    if (node.end == undefined) {
			node.title += "<br/><b>Date:</b> " + node.start;
		    }
		    else {
			node.title += "<br/><b>Start:</b> " + node.start + "<br/><b>End:</b> " + node.end;
		    }
		    if (node.link != undefined) {
			node.content =  "<a href='" + node.link + "'>" + node.content + "</a>"
		    }
		});
		return nodes;
	    };

	    var timeline = new vis.Timeline(container, setTitles(items), groups, options);
	}
    };
    xhr.send();
}
