#include <stdlib.h>
#include <stdio.h>

#define N 32

void set(char *values, int size, char c)
{
    for (int i = 0; i < size; ++i) {
       values[i] = c;
    }
    values[size-1] = '\0';
}

int main(int argc, char **argv)
{
    char *val1 = malloc(sizeof(char[N]));
    char *val2 = malloc(sizeof(char[N]));
    char val3[N];
    char val4[N];

    set(val4, N+1,  '4');
    set(val3, N+2,  'a');
    set(val2, N+17, 'z');
    set(val1, N+18, '1');

    printf("%ld\n", val2-val1);
    printf("%ld\n", val4-val3);
    printf("val1; %s\n", val1);
    printf("val2; %s\n", val2);
    printf("val3; %s\n", val3);
    printf("val4; %s\n", val4);

    return 0;
}
