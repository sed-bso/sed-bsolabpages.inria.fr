
# Microservices

## Networking
docker network create --driver bridge alcyone-nw
docker network rm alcyone-nw

## sf-store (MySQL db)
docker build -t sf-store-img ./seqfeature-store

docker run -d -e MYSQL_USER=seqfeature -e MYSQL_PASSWORD=seqfeature -e MYSQL_DATABASE=seqfeature -e MYSQL_ROOT_PASSWORD=seqfeature --name sf-store --network=alcyone-nw  -p 3306:3306 sf-store-img

## PHPmyadmin
docker run -d --network=alcyone-nw -e PMA_HOST=sf-store --name myadmin -p 8010:80 phpmyadmin/phpmyadmin

## sf-loader (chargement des données dans la bdd)
docker build -t sf-loader-img ./seqfeature-loader

docker run --rm -e MYSQL_USER=seqfeature -e MYSQL_PASSWORD=seqfeature -e MYSQL_DATABASE=seqfeature -e SF_STORE=sf-store -v /home/lschmitt/mb/data:/var/gff:Z --network=alcyone-nw --name sf-loader sf-loader-img

## seagull (service web de gestion des images/conteneurs)
docker run -d -p 10086:10086 --name seagull -v /var/run/docker.sock:/var/run/docker.sock:Z tobegit3hub/seagull

# Macroservices

## Galaxy par défaut (environnement multiservice d'analyse de données biologiques)
docker run -d -p 8080:80 -p 8021:21 -p 8022:22 -p 9002:9002 bgruening/galaxy-stable

## version de test
docker build -t galaxy-vanilla-img --build-arg GALAXY_CONFIG_MASTER_API_KEY=buildkey ./galaxy-base

## version spécialisée (contenant les outils nécessaires pour la génomique comparative)
docker build -t galaxy-comparative_genomics-img --build-arg FLAVOR=comparative_genomics --build-arg GALAXY_CONFIG_MASTER_API_KEY=buildkey ./galaxy-base

## exécution
docker run --name galaxy -d --network=alcyone-nw -e GALAXY_CONFIG_ADMIN_USERS=alcyone@alcyone -e  GALAXY_CONFIG_MASTER_API_KEY=masterkey  -p 8020:80 -p 8023:9002 -p 8022:22 -v /path/to/data:/var/data:Z galaxy-vanilla-img

docker run --name galaxy-loader --network=alcyone-nw -e GALAXY_CONFIG_ADMIN_USERS=alcyone@alcyone -e  GALAXY_CONFIG_MASTER_API_KEY=masterkey -e GALAXY_HOST=galaxy --volumes-from galaxy galaxy-loader-img
