pipeline {
        agent none

        stages {
                stage('first') {
                        steps {
                                node('OS_X_12') {
                                        sh 'uname -a'
                                }
                        }
                }
                stage('second') {
                        parallel {
                                stage('second1') {
                                        steps {
                                                node('autotools') {
                                                        sh 'uname -a'
                                                }
                                        }
                                }
                                stage('second2') {
                                        steps {
                                                node('windows') {
                                                        bat 'echo test'
                                                }
                                        }
                                }
                        }
                }
        }
}
