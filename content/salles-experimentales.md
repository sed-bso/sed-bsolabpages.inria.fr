---
title: Salles Experimentales
toc: true
url: "/salles-experimentales"
image: "img/GV-2.png"
summary: "Espace pour demander l'accès aux salles expérimentales et aux ateliers."
---

<!-- {{< table_of_contents >}}  -->

## <span style="color:#67b189"> Demande d'accès </span>

Pour toute demande d'accès aux salles expérimentales et aux ateliers merci de remplir le formulaire ci-dessous. Un plan du plateau experimental est disponible [ci-dessous](#plan-du-plateau-experimental). La date de fin de contrat nous permet de définir une date limite d'accès aux salles.

Une fois le formulaire rempli, un ticket Gitlab sera pré-rempli. Il vous suffira de vous connecter avec votre compte Gitlab INRIA et de cliquer sur "Create Issue" pour soumettre votre ticket. Les membres du SED seront alors notifiés de votre demande et la traiteront dans les plus brefs délais.


⚠️ L'accès au moyen volume nécéssite de participer à un atelier de sensibilisation sur les risques présents dans cette salle. Il en est de même pour l'utilisation des ateliers électronique et mécanique.

<div style="border:solid; padding:25px;">
    

<form action="https://gitlab.inria.fr/sed-bso/acces_salles_experimentales/-/issues/new?" method="get" class="form-example" id="myform">
<label>
        Prénom NOM
        <input type="text" id="name" style="display:flex" required />
    </label>
    <br>
    <label>
        Équipe
        <input type="text" id="equipe" style="display:flex" required />
    </label>
    <br>
    <label id="contrat">
        Contrat:
         <ul style="list-style-type: none;">
            <li>
                <input type="checkbox" id="contrat_stage" name="contrat"/>
                <label for="contrat_stage"> Stagiaire</label>
            </li>
            <li>
                <input type="checkbox" id="contrat_doc_postdoc_cdd" name="contrat"/>
                <label for="contrat_doc_postdoc_cdd"> Doc/ Postdoc / CDD </label>
            </li>
            <li>
                <input type="checkbox" id="contrat_permanent" name="contrat"/>
                <label for="contrat_permanent"> Permanent</label>
            </li>
        </ul>
    <label id="contract_date">
        Fin de contrat:
        <input type="date" id="contract_end" required/> 
    </label>
    <br>
    <label>
        Salles demandées:
        <ul style="list-style-type: none;">
            <li>
                <input type="checkbox" id="petit_volume" />
                <label for="petit_volume"> Petit volume</label>
            </li>
            <li>
                <input type="checkbox" id="moyen_volume" />
                <label for="moyen_volume"> Moyen volume</label>
            </li>
            <li>
                <input type="checkbox" id="grand_volume" />
                <label for="grand_volume"> Grand volume</label>
            </li>
            <li>
                <input type="checkbox" id="mezzanine" />
                <label for="mezzanine"> Mezzanine</label>
            </li>
            <li>
                <input type="checkbox" id="atelier_elec" />
                <label for="atelier_elec"> Atelier électronique</label>
            </li>
            <li>
                <input type="checkbox" id="atelier_meca" />
                <label for="atelier_meca"> Atelier mécanique</label>
            </li>
        </ul>
    </label>
    <br>
    <label style="display: flex; align-items: start;"> 
        <textarea id="commentaire" cols="70" rows="5" placeholder="Commentaire"></textarea>   
    </label>
    <div class="form-example">
        <input type="text" name="issue[title]" id="title" hidden />
    </div>
    <div class="form-example">
        <input type="text" name="issue[description]" id="description" required hidden />
    </div>
    <br>
    <div style="text-align: center;">
        <input type="submit" value="Soumettre" id="soumettre"  />
    </div>
</form>
</div>
<script>
    var button = document.getElementById('soumettre');
    button.onclick = function () {
        let allAreFilled = true;
        let contratValueCheck = false;				          
        document.getElementById("myform").querySelectorAll("[required]").forEach(function(i) {
            if(!i.value)
            {
                allAreFilled = false;
                return; 
            }
        })
        document.getElementById("contrat").querySelectorAll("[name=contrat]").forEach(function(i) {
            if(i.type == "checkbox") {
                    if(i.checked) {
                        contratValueCheck = true;
                    }
            }
        })
        allAreFilled = contratValueCheck;
        if(!contratValueCheck) {
            document.getElementById('contrat_stage').required = true;
            return;
        }
        else
        {
        var title = document.getElementById("title");
        var name = document.getElementById('name');
        var equipe = document.getElementById('equipe');
        var location = document.getElementById('location');
        var description = document.getElementById('description');
        contract_end = document.getElementById('contract_end')
        commentaire = document.getElementById("commentaire");
        var fonction = ""
        if (document.getElementById('contrat_stage').checked) {
            fonction = "stagiaire";
        }
        if (document.getElementById('contrat_doc_postdoc_cdd').checked) {
            fonction = "doc, postdoc, cdd";
        }   
        if (document.getElementById('contrat_permanent').checked) {
            fonction = "permanent";
        }
        var salles = ""
        if (document.getElementById('grand_volume').checked) {
            salles += "<li>Grand volume </li>"
        }
        if (document.getElementById('mezzanine').checked) {
            salles += "<li>Mezzanine </li>"
        }
        if (document.getElementById('moyen_volume').checked) {
            salles += "<li>Moyen volume </li>"
        }
        if (document.getElementById('petit_volume').checked) {
            salles += "<li>Petit volume </li>"
        }
        description.value = 'Nom: ' + name.value + "<br> Équipe: " + equipe.value + "<br>Fonction: " + fonction +"<br>Fin d'accès: " + contract_end.value + " <br>Salles : <ul>" + salles + "</ul>";
        if (commentaire.value != null) {
            description.value += "<br>Commentaire: " + commentaire.value
        }
        title.value = "Demande d'accès " + name.value
        document.getElementById('myform').submit();
        }
    };    
</script>

<script>
// the selector will match all input controls of type :checkbox
// and attach a click event handler 
$("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);

    if ($box.attr("id") == "contrat_stage" || $box.attr("id") == "contrat_doc_postdoc_cdd" )
    {
        document.getElementById('contract_end').required = true;
        document.getElementById('contract_date').hidden = false;
    }
    else
    {
        document.getElementById('contract_end').required = false;
        document.getElementById('contract_date').hidden = true;
    }
  } else {
    $box.prop("checked", false);
  }
});
</script>

## <span style="color:#67b189"> Plam du plateau expérimental </span>

![plan](/img/plan_salles_expe.png)

## <span style="color:#67b189"> Réservation des salles expérimentales </span>

Il est conseillé de réserver les salles expérimentale lorsque vous avez besoin de faire une experience nécéssitant du calme, une configuration particulière de la salle, etc. Chacune des salles experimentales possèdent un calendrier zimbra consultable ici:

[Petit volume](https://zimbra.inria.fr/home/bordeaux-pdriv_b117@zimbra-local.inria.fr/Calendar.html) (bordeaux-pdriv_b117)

[Moyen volume, salle robot](https://zimbra.inria.fr/home/bordeaux-pdriv_b120_robot@zimbra-local.inria.fr/Calendar.html) (bordeaux-pdriv_b120_robot)

[Moyen volume, salle laser](https://zimbra.inria.fr/home/bordeaux-pdriv_b120_appt@zimbra-local.inria.fr/Calendar.html) (bordeaux-pdriv_b120_appt)

[Grand volume](https://zimbra.inria.fr/home/bordeaux-pdriv_b115_down@zimbra-local.inria.fr/Calendar.html) (bordeaux-pdriv_b115_down)

[Mezzanine](https://zimbra.inria.fr/home/bordeaux-pdriv_b115_up@zimbra-local.inria.fr/Calendar.html) (bordeaux-pdriv_b115_up)

Pour un accès rapide à ces calendriers, vous pouvez les rajouter directement dans votre calendrier zimbra.

⚠️ Lorsque vous faites votre réservation, pensez à préciser les motifs.

## <span style="color:#67b189"> Canaux de communications </span>

Afin de facilité la communication inter-équipe autour des ateliers et des plateformes expérimentales, un canal de communication est disponible via le Mattermost INRIA sur le serveur https://mattermost.inria.fr/salleexpebso.

Chaque espace possède son canal de communication:

[Atelier Électronique](https://mattermost.inria.fr/salleexpebso/channels/atelier-electronique)

[Atelier Mécanique](https://mattermost.inria.fr/salleexpebso/channels/atelier-mecanique)

[Petit Volume](https://mattermost.inria.fr/salleexpebso/channels/petit-volume)

[Moyen Volume](https://mattermost.inria.fr/salleexpebso/channels/moyen-volume)

[Grand Volume & Mezzanine](https://mattermost.inria.fr/salleexpebso/channels/grand-volume)
