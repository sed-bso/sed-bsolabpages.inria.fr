---
title: Les Actions Transversales
url: /action-transverses

toc: true
image: "img/unsplash-photos-TzMi5Ov7QeM-2.jpg"
credit: "https://unsplash.com/photos/TzMi5Ov7QeM"
thumbnail: "img/unsplash-photos-TzMi5Ov7QeM.tn-500x500-2.jpg"
---

Les actions transversales concernent plusieurs sujets :
* les plateformes expérimentales
* les formations à des technos spécifiques via **Les Midis de la Bidouille**
* les actions de formations
* l'accès aux logiciels scientifiques à licenses (en coordination avec la DSI)


<!--more-->

## <span style="color:#67b189"> Les Plateformes Expérimentales</span>
### <span style="color:#4c8667"> Les Plateformes Interaction Humain - Systèmes Numériques</span>

Le SED gère les espaces expérimentaux dédiés aux intercations Humaines - Système Numérique et les ateliers mécanique et électronique permettant la réalisation de prototypes.

Avec ces espaces, le SED accompagne, coordonne les actions scientifiques qui y sont hébergées au plus proche des préoccupations des chercheuses et chercheurs :
1. La coupole
2. Les expériences BCI
3. Hobit
4. Une cabine Insonorisée
5. Une table Optique
6. Le Grand Volume avec l'expérience autour des Avatars
7. et même parfois des pièces innatendue comme le local Syndical...

### <span style="color:#4c8667"> La Plateforme PlaFRIM</span>

PlaFRIM est une plateforme expérimentale pilotée par le SED de Bordeaux à la base, sous la direction scientifique de Brice Goglin (DR TADaaM) et la responsabilité technique de Julien Lelaurain (DSI).

La plateforme a pour vocation de :
1. créer de la transversalité entre les équipes de Mathématiques et d'informatique ;
2. être en avance de phase sur les matériels pour préparer les objets logiciels aux plateformes de calcul Tier-0 et Tier-1 de demain.

  [![Lien vers la page Web du projet]()](https://www.plafrim.fr)


---------------
## <span style="color:#67b189"> Les Midis de la Bidouille</span>
---------------
<details>
<summary> Les bidouilles présentées </summary>

  <details>
  <summary> [16/04/2024] - La conteneurisation avec Docker (Pau)
  </summary>

  ```
  author: "Algiane Froehly"
  ```
 [Docker](https://www.docker.com/) est un logiciel  de conteneurisation qui permet notamment de déployer des environnements locaux.

Entre autre utilités, Docker peut vous aider :
  - à vous rapprocher de la configuration d’une ou d'un collègue pour répliquer le comportement d’un code (bug) sans modifier votre installation;
  - à faire cohabiter sereinement plusieurs configurations et versions de piles logicielles;
  - à bénéficier d’outils non disponible sur votre OS (par exemple valgrind pour les utilisateurs de Mac);
  - ou encore à déployer rapidement des environnements donnés dans votre processus d’intégration continue.

La fin de la formation aborde très rapidement quelques notions d’architecture matérielle et d’émulation : vous savez que vous devez télécharger des binaires x86_64 pour votre ordi mais pas vraiment pourquoi?  Vous ne comprenez pas pourquoi le binaire envoyé par votre collègue ne tourne pas chez vous et vous aimeriez bien l’utiliser quand même?
En une ligne de commande, le déploiement grace à Docker d’une image de l’outil d’émulation [Qemu](https://www.qemu.org/)  peut vous fournir une solution.

  [![Tutoriel]()](https://notes.inria.fr/s/npRkaaXHx)

  </details>

  <details>
  <summary> [07/11/2023] - Au-delà de ChatGPT : Explorer Mistral et les LLMs Open-Source
  </summary>

  ```
  author: "Yannis Bendi-Ouis"
  ```

  [![Présentation]()](https://docs.google.com/presentation/d/10mFNXwXqqMzSngehPzcqCmlVmddaj2-5k1jBiAQRthM/edit#slide=id.g29dc612e7e9_0_151)
  [![Local-LLMs]()](https://github.com/Naowak/Local-LLMs/tree/main)
  [![LM Studio]()](https://lmstudio.ai/)

  </details>

  <details>
  <summary> [05/12/2023] - Faites votre site web perso avec Hugo
  </summary>

  ```
  author: "Lucas Joseph"
  ```

  [![Tutoriel]()](https://notes.inria.fr/s/a9pYBQCsM)

  </details>

  <details>
  <summary> [12/09/2023] - Outils libres de déverminage
  </summary>

  ```
  author: "Luca Cirrottola"
  ```

  [![Tutoriel]()](https://gitlab.inria.fr/lcirrott/horror)

  </details>

  <details>
  <summary> [18/07/2023] - Dans les coulisses de Pytorch
  </summary>

  ```
  author: "Zhe Li"
  ```

  [![Presentation]()](/formations/MdB/pdf/Pytorch.pdf)
  [![google-colab]()](https://colab.research.google.com/drive/1pNHZLXSPhe0Mobr9e8-PVc1CF8YzOcT3?usp=sharing)

  </details>

  <details>
  <summary> [27/06/2023] - La découverte de LLVM épisode 4 : instrumentation de binaires avec Clang
  </summary>

  ```
  author: "Philippe Virouleau"
  ```

  [![bidouille-instrumentation]()](https://gitlab.inria.fr/viroulea/bidouille-instrumentation)

  </details>

  <details>
  <summary> [13/06/2023] - Gallerie d'exemples Gitlab-CI
  </summary>

  ```
  author: "Denis Arrivault et Florent Pruvost"
  ```

  [![gitlabci-gallery]()](https://gitlab.inria.fr/gitlabci_gallery/intro)

  </details>

  <details>
  <summary> [02/05/2023] - Getting started with Git </summary>

  ```
  author: "Ludovic Courtez & Algiane Froehly"
  ```
  [![html]()](/formations/git-basics-Pau-02-05-2023/git-basics-Pau-02-05-2023.html)
  [![org]()](/formations/MdB/org/git-basics-Pau-02-05-2023/git-basics-Pau-02-05-2023.org)
  </details>


  <details>
  <summary> [03/05/2022] - Medit: fast and light mesh inspection and vizualization </summary>

  ```
  author: "Algiane Froehly"
  ```
   Medit est une application légère et rapide permettant
   l'inspection et la visualisation de maillages.
   Medit permet entre autre : de rechercher une entité
   d'indice donné dans un maillage et d'afficher les informations
   associées; de réaliser facilement des plans de coupe pour visualiser
   des entités volumiques ou surfaciques; de lier des vues de maillages
   pour les comparer; d'exploser la vue du maillage pour détecter des
   entités dégénérées et de visualiser des champs de solutions.

  [![html]()](/formations/MdB-Medit/MdB-Medit.html)
  [![pdf]()](/formations/MdB-Medit/MdB-Medit.pdf)
  [![org]()](/formations/MdB/org/MdB-Medit/MdB-Medit.org)
  </details>

  <details>
  <summary> [03/03/2022] - Auto-hébergement </summary>

  ```
  author: "Philippe Swartvagher"
  ```

  [![auto-hebergement]()](/formations/auto-hebergement-20220303.pdf)
  </details>


  <details>
  <summary> [02/12/2021] - Pytorch partie 2 : Applications (régression linéaire et classification d'images avec un CNN)  </summary>

  ```
  author: "Eloïse Guillem"
  ```

  [![pytorch]()](http://sed.bordeaux.inria.fr/pytorch-20211021/applications/)
  </details>


  <details>
  <summary> [25/11/2021] - energy_scope: mesurer le profil énergétique d'une application HPC </summary>


  ```
  author: "Hervé Mathieu"
  ```
  [![pytorch]()](https://gitlab.inria.fr/sed-bso/datacenter/-/blob/master/2021_11_mdb_energy_scope/2021_11_mdb_energy_scope.md)
  </details>

  <details>
  <summary> [16/11/2021] - CMake: tour d'horizon des fonctionnalités à partir d'exemples, trucs et astuces </summary>


  ```
  author: "Florent Pruvost (& Marc Fuentes)"
  ```
  [![html]()](https://sed-bso.gitlabpages.inria.fr/cmake/)
  [![Sources Gitlab]()](https://gitlab.inria.fr/sed-bso/cmake)
  </details>

  <details>
  <summary> [21/10/2021] - Initiation à PyTorch: création de tenseurs, gestion de dérivées etc... </summary>


  ```
  author: "Éloïse Guillem"
  ```
  [![Sources]()](http://sed.bordeaux.inria.fr/pytorch-20211021/initiation/)
  </details>

  <details>
  <summary> [31/03/2020] - Introduction à Pandas et Scikit Learn </summary>

  ```
  author: "Rémi Duclos"
  ```
  [![Sources]()](https://gitlab.inria.fr/fuentes/cahiers-jupyter/-/tree/master/pandas-scikit)
  </details>

  <details>
  <summary> [14/02/2020] - Modules (tcl) pour les grappes de calcul </summary>

  ```
  author: "Nathalie Furmento & Brice Goglin"
  ```
  [![org]()](https://www.labri.fr/perso/furmento/bidouille/modules/modules.org)
  [![pdf]()](https://www.labri.fr/perso/furmento/bidouille/modules/modules.pdf)
  </details>

  <details>
  <summary> [28/01/2020] - Inkscape (Pau) </summary>

  ```
  author: "Mathieu Haefele"
  ```
  [![html]()](/formations/MdB/inkscape/talk.html)
  </details>

  <details>
  <summary> [21/01/2020] - Modèle «Roofline» </summary>

  ```
  author: "Mathieu Haefele"
  ```
  [![html]()](/formations/MdB/roofline/public/talk.html)
  </details>

  <details>
  <summary> [14/01/2020] - Vim pour la programmation (Pau) </summary>

  ```
  author: "Marc Fuentes"
  ```
  [![html]()](/formations/vim_prog.html)
  </details>

  <details>
  <summary> [03/12/2019] - Javascript et NodeJs </summary>

  ```
  author: "Thibault Lainé"
  ```
  [![Source]()](https://gitlab.inria.fr/sed-bso/mdb-javascript)
  </details>

  <details>
  <summary> [15/11/2019] - Impression 3D avec OpenSCAD </summary>

  ```
  author: "Brice Goglin"
  ```
  [![org]()](/formations/MdB/org/openscad.org)
  [![html]()](/formations/openscad.html)
  </details>

  <details>
  <summary> [18/10/2019] - Impression 3D : modèles STL </summary>

  ```
  author: "Brice Goglin & David Sherman"
  ```
  A partir d'un modèle STL (récupéré en ligne [1] ou conçu soi-même [2]), nous verrons comment un logiciel de découpe (comme Slic3r [3]) traduit ce modèle en couches plus ou moins denses avant de l'envoyer à l'imprimante. On abordera les notions de support et de jupe qui peuvent être nécessaires selon la forme du modèle.
  </details>

  <details>
  <summary> [09/09/2019] - Interfaces C-Python-Fortran </summary>

  ```
  author: "Marc Fuentes"
  ```
  [![Source]()](https://gitlab.inria.fr/fuentes/mdb_inter)
  </details>

  <details>
  <summary> [13/06/2019] - Introduction au Langage R </summary>

  ```
  author: "Dan Dutartre"
  ```
  [![Source]()](/formations/mdb_R.7z)
  [![html]()](/formations/Intro_R.html)
  </details>

  <details>
  <summary> [24/05/2019] - Visualisation avec Kibana </summary>

  ```
  author: "Hervé Mathieu"
  ```
  [![Markdown]()](/formations/MdB/elastic_search.md)
  [![html]()](/formations/elastic_search.html)
  </details>

  <details>
  <summary> [11/04/2019] - Notebooks Jupyter </summary>

  ```
  author: "Marc Fuentes"
  ```
  [![Notebook]()](/formations/intro_jupyter.ipynb)
  [![html]()](/formations/intro_jupyter.html)
  </details>

  <details>
  <summary> [28/03/2019] - Déverminage avec valgrind
  </summary>

  ```
  author: "Cyril Bordage"
  ```
  Outil de programmation libre pour déboguer, effectuer du profilage de code et mettre en évidence des fuites mémoires.

  [![org]()](/formations/MdB/org/valgrind.org)
  [![html]()](/formations/valgrind.html)
  [![Source]()](/formations/test.c)
  </details>

  <details>
  <summary> [28/02/2019] - Bonnes pratiques CMake moderne </summary>

  ```
  author: "Florent Pruvost"
  ```
  [![org]()](/formations/MdB/org/modern_cmake_guidelines.org)
  [![html]()](/formations/modern_cmake_guidelines.html)
  </details>

  <details>
  <summary> [17/01/2019] - Déverminage avec GDB </summary>

  ```
  author: "Marc Fuentes"
  ```
  [![org]()](/formations/MdB/org/gdb_2019.org)
  [![html]()](/formations/gdb_2019.html)
  </details>

  <details>
  <summary> [06/12/2018] - Git, le retour </summary>

  ```
  author: "Ludovic Courtès"
  ```
  [![org]()](/formations/MdB/org/git-advanced.org)
  [![html]()](/formations/git-advanced.html)
  </details>

  <details>
  <summary> [05/11/2018] - Introduction à Git </summary>

  ```
  author: "Ludovic Courtès"
  ```
  [![org]()](/formations/MdB/org/git-basics.org)
  [![html]()](/formations/git-basics.html)
  </details>

  <details>
  <summary> [28/09/2018] - Astuces shell </summary>

  ```
  author: "Cyril Bordage"
  ```
  [![fichier texte de commandes]()](/formations/shell_astuces.txt)
  </details>

  <details>
  <summary> [22/06/2018] - Présentation du Langage Rust </summary>

  ```
  author: "Olivier Saut"
  ```
  [![fichier pdf]()](/formations/presentation_rust.pdf)
  </details>

  <details>
  <summary> [05/06/2018] - Programmation de jeux vidéos avec SDL2
  </summary>

  ```
  author: "Raymond Namyst"
  ```
  Programmation d'un jeu avec défilement : manipulations de «sprites», gestion du son, etc...

  [![planches pdf]()](/formations/sdl2.pdf)
  </details>

  <details>
  <summary> [03/05/2018] - Jenkins Pipelines
  </summary>

  ```
  author: "Brice Goglin"
  ```
  Mise-en-œuvre des pipelines Jenkins sur la plate-forme d'intégration continue de l'INRIA

  [![exemple fichier groovy]()](/formations/ex1.groovy)
  [![lien vers les fichiers groovy de hwloc]()](https://github.com/bGoglin/hwloc/tree/master/contrib/ci.inria.fr)
  </details>

  <details>
  <summary> [24/04/2018] - OpenMP avancé (tâches)
  </summary>

  ```
  author: "Pierre-André Wacrenier"
  ```
  Utilisation avancée d'OpenMP avec notamment les tâches, illustration sur le problème du voyageur de commerce

  [![sujet et sources]()](/formations/openmp-tasks.tgz)
  </details>

  <details>
  <summary> [03/04/2018] - OpenMP, un paradigme pour le multi-cœurs
  </summary>

  ```
  author: "Pierre-André Wacrenier"
  ```
  Découverte d'OpenMP pour le parallèlisme à mémoire partagée : création de fils d'exécution, sections critiques, opérations atomiques, réductions

  [![sujet et sources]()](/formations/openmp.tgz)
  </details>

  <details>
  <summary> [20/03/2018] - Le logiciel de chiffrement GnuPG
  </summary>

  ```
  author: "Andreas Enge"
  ```
  Utilisation de GnuPG : création de clefs, signature, chiffrement, déchiffrement

  [![planches pdf]()](/formations/gnupg.pdf)
  [![commandes GnuPG pour mutt ]()](/formations/gnupg_muttrc.txt)

  </details>

  <details>
  <summary> [15/03/2018] - Le gestionnaire de paquets guix
  </summary>

  ```
  author: "Ludovic Courtès"
  ```
  Utilisation du gestionnaire de paquets guix sur la plate-forme plafrim

  [![org]()](/formations/MdB/org/guix.org)
  [![html]()](/formations/guix.html)

  </details>

  <details>
  <summary> [06/02/2018] - Julia, langage de programmation numérique
  </summary>

  ```
  author: "Issam Tahiri"
  ```

  [![lien vers github]()](https://github.com/Issamt/julia_towards_1.0)

  </details>

  <details>
  <summary> [23/01/2018] - Utilisation avancée de git
  </summary>

  ```
  author: "Ludovic Courtès"
  ```
  Utilisation avancée de git : rebase

  [![org]()](/formations/MdB/org/git-rebase.org)
  [![html]()](/formations/git-rebase.html)

  </details>

  <details>
  <summary> [09/01/2018] - Introduction à gnu gdb
  </summary>

  ```
  author: "Marc Fuentes"
  ```

  [![org]()](/formations/MdB/org/gdb.org)
  [![html]()](/formations/gdb.html)

  </details>

  <details>
  <summary> [23/11/2017] - Découvrez mpi avec python
  </summary>

  ```
  author: "Marc Fuentes"
  ```
  On passe en revue les fonctions de base de l'api mpi a l'aide de python

  [![org]()](/formations/MdB/org/mpi4py.org)
  [![html]()](/formations/mpi4py.html)

  </details>

  <details>
  <summary> [xx/xx/2017] - Introduction à CMake
  </summary>

  ```
  author: "Marc Fuentes"
  ```

  [![org]()](/formations/MdB/org/cmake.org)
  [![html]()](/formations/cmake.html)

  </details>

  <details>
  <summary> [xx/xx/2017] - gitlab-ci
  </summary>

  ```
  author: "Florent Pruvost"
  ```
  Présentation de gitlab-ci, le système d'intégration continue de la plateforme nationale inria gitlab

  [![org]()](/formations/MdB/org/gitlab-ci-nojenkins.org)
  [![html]()](/formations/gitlab-ci-nojenkins.html)

  </details>

  <details>
  <summary> [xx/xx/2017] - Sonarqube
  </summary>

  ```
  author: "Florent Pruvost, Hervé Mathieu"
  ```
  Introduction au tableau de bord sonarqube pour améliorer la qualité du code

  </details>

  <details>
  <summary> [23/05/2017] - Docker
  </summary>

  ```
  author: "Louise-Amélie Schmitt, David Sherman"
  ```

  [![planches pdf David]()](/formations/presentationDocker.pdf)
  [![commandes shell Amélie]()](/formations/commandesAmelie.sh)

  </details>

  <details>
  <summary> [23/03/2017] - Interactions gitlab/Jenkins
  </summary>

  ```
  author: "Florent Pruvost, Marc Fuentes"
  ```
  Utilisation conjointe des plate-formes gitlab et ci /jenkins

  [![org]()](/formations/MdB/org/gitlab-ci.org)
  [![html]()](/formations/gitlab-ci.html)

  </details>

  <details>
  <summary> [16/03/2017] - gitlab
  </summary>

  ```
  author: "François Rué"
  ```
  Présentation de la plate-forme nationale gitlab : projets, utilisateurs, permissions, demandes de fusion (mr), etc...

  </details>

  <details>
  <summary> [09/02/2017] - Bonnes pratiques collaboratives avec gits
  </summary>

  ```
  author: "Ludovic Courtès"
  ```
  Présentation des méthodes collaboratives de travail avec git

  [![org]()](/formations/MdB/org/git-workflow.org)
  [![html]()](/formations/git-workflow.html)

  </details>

  <details>
  <summary> [26/01/2017] - zsh
  </summary>

  ```
  author: "Cyril Bordage"
  ```
  Présentation du shell zsh : complétion, expansion de noms de fichiers et greffons externes

  [![org]()](/formations/MdB/org/zsh.org)
  [![html]()](/formations/zsh.html)

  </details>

  <details>
  <summary> [15/12/2016] - Vim pour la programmation
  </summary>

  ```
  author: "Marc Fuentes"
  ```
  Présentation de divers outils (ctags, fugitive, grep) pour utiliser vim commenvironnement de développement intégré

  [![org]()](/formations/MdB/org/vim.org)
  [![html]()](/formations/vim.html)

  </details>

  <details>
  <summary> [10/10/2016] - Openstreetmap
  </summary>

  ```
  author: "Vincent Bergeot"
  ```
  Présentation de la base de données openstreetmap, des divers outils que l'on peut utiliser avec et de comment contribuer?

  [![planches pdf]()](/formations/osm.pdf)

  </details>

  <details>
  <summary> [14/06/2016] - Inkscape
  </summary>

  ```
  author: "Cyril Bordage"
  ```
  Introduction aux fonctions de base de Inkscape

  </details>

  <details>
  <summary> [11/02/2016] - tmux
  </summary>

  ```
  author: "Marc Fuentes"
  ```
  Présentation de l'utilisation  basique de tmux, un multiplexeur de terminal

  [![org]()](/formations/MdB/org/tmux.org)
  [![html]()](/formations/tmux.html)

  </details>

  <details>
  <summary> [14/01/2016] - Raspberry π (ii)
  </summary>

  ```
  author: "Thibault Lainé"
  ```
  Raspberry π : les Raspberry π sont des ordinateurs miniaturisés qui permettent facilement de faire des objets connectés

  [![archive org + images]()](/formations/raspberry.tgz)
  [![html]()](/formations/raspberry/raspberry.html)

  </details>

  <details>
  <summary> [05/11/2015] - Raspberry π
  </summary>

  ```
  author: "Thibault Lainé"
  ```
  Raspberry π : les Raspberry π sont des ordinateurs miniaturisés qui permettent facilement de faire des objets connectés

  [![archive org + images]()](/formations/raspberry.tgz)
  [![html]()](/formations/raspberry/raspberry.html)

  </details>

  <details>
  <summary> [14/09/2015] - git avancé (ii)
  </summary>

  ```
  author: "Brice Goglin"
  ```

  [![org]()](/formations/MdB/org/bidouille-git.org)
  [![html]()](/formations/bidouille-git.html)

  </details>

  <details>
  <summary> [20/08/2015] - Présentation de slurm
  </summary>

  ```
  author: "Redouane Bouchouirbat"
  ```
  Présentation de slurm : slurm est l'ordonnanceur actuellement utilisé sur la nouvelle plate-forme plafrim ii

  </details>

  <details>
  <summary> [11/06/2015] - Introduction à openmp
  </summary>

  ```
  author: "Pierre-1ndré Wacrenier"
  ```
  Introduction à openmp: le cadriciel openmp permet de faire du parallèlisme à
  mémoire distribuée d'une façon accessible en ajoutant des #pragmas à un code
  séquentiel.

  [![archive TP]()](/formations/openmp.tgz)

  </details>

  <details>
  <summary> [20/03/2015] - Présentation d'orgmode (ii)
  </summary>

  ```
  author: "Ludovic Courtès"
  ```
  Présentation d'orgmode : une extension de gnu emacs permettant de gérer des
  listes, des agendas ou de planifier des projets

  [![org]()](/formations/MdB/org/org.org)
  [![html]()](/formations/org.html)

  </details>

  <details>
  <summary> [03/02/2015] - Utilisation de ssh
  </summary>

  ```
  author: "Guillaume cassonnet"
  ```
  Présentation de l'outil <b>ssh</b>, avec notamment
  les tunnels avant, arrière, le transfert de fichier et les agents ssh

  [![org]()](/formations/MdB/org/ssh_gui.org)
  [![html]()](/formations/ssh_gui.html)

  </details>

  <details>
  <summary> [16/01/2015] - Déverminage parallèle
  </summary>

  ```
  author: "Cédric Lachat"
  ```
  Une introduction au déverminage parallèle (mpi) en utilisant des outils tels que valgrind et gnu gdb

  [![org]()](/formations/MdB/org/gdb-valgrind-MPI.org)
  [![html]()](/formations/gdb-valgrind-MPI.html)

  </details>

  <details>
  <summary> [05/12/2014] - git avancé
  </summary>

  ```
  author: "Brice Goglin"
  ```
  Utilisation avancée de git avec notamment :
  <ul>
  <li>commandes de bases: show, status, log, commit, add, pull, push, merge, branch.</li>
  <li>les bases des branches locales et distantes.</li>
  <li>résoudre les conflits dans pull ou merge.</li>
  <li>connaître la différence entre les changements staged ou non.</li>
  <li>les descripteurs de commits head^^, head~5, etc.</li>
  <li>commandes avancées : rebase, cherry-pick, reset, stash, reflog, clean, blame,</li>
  </ul>

  [![org]()](/formations/MdB/org/bidouille-git.org)
  [![html]()](/formations/bidouille-git.html)

  </details>

  <details>
  <summary> [07/11/2014] - Utilisation de awk
  </summary>

  ```
  author: "Nathalie Furmento"
  ```

  [![org]()](/formations/MdB/org/awk.org)
  [![html]()](/formations/awk.html)

  </details>

  <details>
  <summary> [16/10/2014] - Utilisation de gnu emacs pour le développement
  </summary>

  ```
  author: "Ludovic Courtès"
  ```

  [![org]()](/formations/MdB/org/emacs_dev.org)
  [![html]()](/formations/emacs_dev.html)

  </details>

  <details>
  <summary> [02/10/2014] - Outils shell de base
  </summary>

  ```
  author: "Cédric Lachat"
  ```
  </details>


</details>

---------------
## <span style="color:#67b189"> Accès aux logiciels scientifiques à licenses</span>
---------------
Le SED et la DSI (Direction des systèmes d'information) offrent un accès à un
certain nombre de logiciels scientifiques à licences.
Pour la plupart de ces logiciels, l'offre est mutualisée au niveau national.

### <span style="color:#4c8667"> Matlab</span>
La documentation d'installation et d'utilisation de **Matlab-r2020b** est
disponible sur gitlab : https://gitlab.inria.fr/sed-bso/matlab.

L'achat de nouvelles licenses est en cours de reflexion (grâce à nos collègues
de Grenoble, Rennes et Paris).

Afin de préparer au mieux les discussions entre le service achat INRIA et le
service commercial de Mathworks, les utilisateurs et usages de Matlab sont
actuellement recensés via un sondage ouvert jusqu'au **15 mars 2025** :
https://sondages.inria.fr/index.php/348178?lang=fr.

### <span style="color:#4c8667"> Autres logiciels</span>
Les information d'accès et d'utilisation des autres logiciels sont disponibles
sur le site de la documentation SI  (VPN requis
si vous n'êtes pas sur un réseau Inria) : https://doc-si.inria.fr/display/SU/Logiciels+scientifiques.

---------------
## <span style="color:#67b189"> La présentation des travaux du SED</span>
---------------

<details>
<summary> 30 mai 2024 </summary>

  [![introduction]()](/presentations/sed.pdf)

  [![pédagogie]()](/presentations/presentation-sed-2024-1.pdf)

  [![recherche]()](/presentations/presentation_Sante.pdf)
  [![recherche]()](/presentations/presentation-compose-2.pdf)
  [![recherche]()](/presentations/presentation-scotch.pdf)

  [![transfert]()](/presentations/presentation-qontrol.pdf)
  [![transfert]()](/presentations/presentation-guix.pdf)

</details>

<details>
<summary> 24 juin 2022 </summary>

  [![introduction]()](/presentations/introduction.pdf)

  [![distribution et déploiement]()](/presentations/distri.pdf)

  [![graphes, partitionnement, maillage]()](/presentations/mmgSkotx.pdf)

  [![algèbre linéaire]()](/presentations/linalg.pdf)

  [![simulation mécanique, modèles]()](/presentations/simu.pdf)

  [![santé numérique, IA]()](/presentations/sante.pdf)

</details>
