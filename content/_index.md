---
title: Service d'Expérimentation et de développement
subtitle: "Inria (universités de Bordeaux & Pau) "
---
[![SED BDX](/img/logo-sed.png)](https://sed.bordeaux.inria.fr)

[Site Institutionnel Inria](https://www.inria.fr).
