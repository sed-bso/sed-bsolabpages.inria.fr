---
title: L'Équipe
url: "/lequipe"
credit: "pixabay.com"
image: "img/photo-team-04.jpg"
thumbnail: img/photo-team-04-500x500.png
---
L'équipe est une source de compétences et de savoir-faire utiles aux scientifiques du centre Inria de l'Université de Bordeaux, couvrant les équipes paloises et bordelaises, travaillant avec les ingénieur·e·s de tout l'écosystème.

<!--more-->


# <span style="color:#67b189"> L'Équipe </span>

* Denis Arrivault - Ingénieur de Recherche -  Filtrage, Monte-Carlo
* Clément Barthélemy - Ingénieur de Recherche - Partitionnment (Consortium Scotch)
* Luca Cirrottola - Ingénieur de Recherche - Calcul Scientifique, HPC
* Aurélien Citrain - Ingénieur de Recherche - Calcul Scientique, Langage C++ avancé
* Ludovic Courtés - Ingénieur de Recherche - Pair à Pair, Reproductibilité
* Dan Dutartre - Ingénieur de Recherche - IA, Stats, Santé Numérique
* Algiane Froehly - Ingénieure de Recherche - Calcul Scientifique, HPC
* Marc Fuentes - Ingénieur de Recherche - Calcul Scientifique, Partitionnement
* Nathalie Furmento - Ingénieure de Recherche - Ordonnancement sur machines distribuées hétérogènes, StarPU
* Romain Garbage - Ingénieur PEPR Numpex - Empaquetage, Reproductibilité
* Eloise Guillem - Ingénieur CAP IA - Pédagogie, Data Scientist
* Lucas Joseph - Ingénieur de Recherche - Robotique, DevLog
* Gilles Marait - Ingénieur de Recherche - Algèbre Linéaire, Langage C++ avancé, Composabilité
* Florent Pruvost - Ingénieur de Recherche - Algèbre Linéaire, Calcul Scientifique, HPC
* Gwladys Ravon - Ingénieure de Recherche - Calcul Scientifique, Santé Numérique
* François Rue - Ingénieur de Recherche - libreoffice, courriel et autres choses
* Anaëlle Zanella - Ingénieure Plan IA - IA, Santé Numérique

----------------------------------------------------------

* Julien Castelneau - Ingénieur de Recherche - Coordinateur Technique PEPR Santé Numérique
* Hervé Mathieu - Ingénieur de Recherche - [![Denergium]()](https://www.denergium.fr/)

----------------------------------------------------------

* Philippe Virouleau - Ingénieur de Recherche - SDT - MC
* Pierre Esterie - Ingénieur PEPR Numpex - Compilation, langages avancés
* Ludo ANDRIANIRINA MAMISOA - Stagiaire - Stats, Santé Numérique

# <span style="color:#67b189"> Nous Contacter </span>

* L'ensemble des éléments pour nous contacter est disponible sur l'Intranet.

<center>
 <a href="https://intranet.inria.fr/Inria/Directions/Innovation/DGDI/SDT-et-SED">Contact - SED de l'Université de Bordeaux et Pau</a>
</center>
