---
title: L'Organisation du Développement Logiciel
url: "/orga-dev"
image: "img/devlog.jpg"
---

Le développement logiciel est une action essentielle du travail des ingénieur·e·s du **Service d'expérimentation et de développement.**

<!--more-->
# <span style="color:#67b189"> Co-Construction de Projets de Recherche </span>

* Pour accompagner l'action de développement logicielle (hors action plateforme IHSN et PlaFRIM), le service peut s'appuyer des instances et commissions :
1. la **commission locale de Développement Technologique**

Courriel : cdt-bordeaux@inria.fr

[Informations locales sur la CDT (ADT, recrutement ingénieurs R&D...)](https://intranet.inria.fr/Inria/Instances/Instances-Bordeaux/Commission-du-Developpement-Technologique/CDT)

[Modèle de Document pour DEMANDE SUPPORT INGÉNIERIE](/projets-dev/BSO-ADT.modèle.pptx)

2. la **commission de Développement logicielle** qui est une instance d'échanges constituée de l'ensemble des RSED (de l'ensemble des centres)

* Pour solliciter le service et envisager de construire un projet, nous préconisons le processus suivant se basant fondamentalement sur l'échange.

 [![Processus métier](/img/processus.png)](/img/processus.png)

 [![Présentation des Travaux du Service - 30 mai 2024](/presentations/sed.pdf)](/presentations/sed.pdf)


# <span style="color:#67b189"> Rapport d'Activité 2023 </span>

Le rapport d'activité de l'année 2023 pour le SED. Ce rapport pointe également vers d'autres documents plus riches sur les activités menées par les membres du service.

 [![Rapport d'Activité 2023](/img/photo-ra-2023-200x680.jpg)](/img/ra2023.pdf)


# <span style="color:#67b189"> Suivi des Projets d'Équipes Recherche </span>
## <span style="color:#4c8667"> Cartographie des projets accompagnés par les Ingénieur.e.s du SED </span>

* Les projets accompagnés bénéficient d'un suivi dans le temps du travail réalisé. Les avancées concernent l'ensemble des travaux de recherche et leurs intégrations au sein de la brique technologique (logiciel, plateforme...).

* Ce suivi concerne les projets accompagnés par les Ingénieur.e.s du Service en cohérence avec la pile logicielle des Équipes Projet du centre Inria de l'Université de Bordeaux et Pau.

<center>
<img src="https://sed-bso.gitlabpages.inria.fr/projets-dev/sed-stack-2.svg" width="543" height="501" border="2" style="border-color: red " usemap="#map" />

<map id="map" name="map">
<!-- #$-:Image map file created by GIMP Image Map plug-in -->
<!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
<!-- #$-:Please do not edit lines starting with "#$" -->
<!-- #$VERSION:2.3 -->
<!-- #$AUTHOR:rue -->
<area shape="rect" coords="66,38,132,81" target="AeroSol" href="http://team.inria.fr/cardamom/aerosol/" />
<area shape="rect" coords="78,105,126,123" href="https://sed-bso.gitlabpages.inria.fr/projets-dev/projet-composyx.html" />
<area shape="rect" coords="133,39,177,81" href="https://sed-bso.gitlabpages.inria.fr/projets-dev/projet-geos.html" />
<area shape="rect" coords="178,39,224,81" href="https://sed-bso.gitlabpages.inria.fr/projets-dev/projet-cardiolib.html" />
<area shape="rect" coords="90,180,126,200" href="https://sed-bso.gitlabpages.inria.fr/projets-dev/projet-pastix.html" />
<area shape="rect" coords="15,180,80,200" href="https://sed-bso.gitlabpages.inria.fr/projets-dev/projet-chameleon.html" />
<area shape="rect" coords="322,168,407,211" href="https://sed-bso.gitlabpages.inria.fr/projets-dev/projet-scotch.html" />
<area shape="rect" coords="196,218,358,246" href="https://sed-bso.gitlabpages.inria.fr/projets-dev/projet-hwloc.html" />
<area shape="rect" coords="12,218,174,243" href="https://starpu.gitlabpages.inria.fr/timeline.html" />
</map>
</center>



